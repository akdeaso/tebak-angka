let skorP1 = 0;
let skorP2 = 0;
let ronde = 1;

const main = () => {
  const resultDiv = document.getElementById("result");
  resultDiv.innerHTML = "";

  const p1Input = getInputValue("player1Input");
  const p2Input = getInputValue("player2Input");

  let ulangi = validasi(p1Input, p2Input);
  if (!ulangi) {
    resultDiv.innerHTML = "<p>Angka tidak boleh sama. Ulangi!</p>";
    return;
  }

  let random = getRandom();
  let winnerMessage = "";

  if (p1Input === random) {
    winnerMessage = "Player 1 Menang";
    skorP1++;
  }
  if (p2Input === random) {
    winnerMessage += (winnerMessage ? " dan " : "") + "Player 2 Menang";
    skorP2++;
  }

  if (winnerMessage) {
    winnerMessage += "!";
  } else {
    winnerMessage = "Hasil Seri";
  }

  const resultMessage = `
    <p>Ronde ${ronde}</p>
    <p>Nilai yang dicari: ${random}</p>
    <p>${winnerMessage}</p>
    <p>-----------------------------------</p>
    <p>Skor Player 1: ${skorP1}</p>
    <p>Skor Player 2: ${skorP2}</p>
  `;

  resultDiv.innerHTML = resultMessage;

  if (ronde === 5) {
    determineWinner();
    ronde = 1;
    skorP1 = 0;
    skorP2 = 0;
  } else {
    ronde++;
  }
};

const getInputValue = (inputId) => parseInt(document.getElementById(inputId).value);

const validasi = (player1, player2) => {
  if (player1 === player2) {
    return false;
  }

  if (player1 < 1 || player2 < 1 || player1 > 3 || player2 > 3 || isNaN(player1) || isNaN(player2)) {
    return false;
  }

  return true;
};

const getRandom = () => {
  const range = [1, 2, 3];
  return range[Math.floor(Math.random() * range.length)];
};

const determineWinner = () => {
  const resultDiv = document.getElementById("result");
  let winnerMessage = "";

  if (skorP1 > skorP2) {
    winnerMessage = "Good Job Player 1";
  } else if (skorP1 < skorP2) {
    winnerMessage = "Good Job Player 2";
  } else {
    winnerMessage = "Wow pertarungan yang sangat sengit";
  }

  resultDiv.innerHTML = `
    <p>${winnerMessage}</p>
    <button onclick="resetGame()">Main Lagi</button>
  `;
};

const resetGame = () => {
  skorP1 = 0;
  skorP2 = 0;
  ronde = 1;
  const resultDiv = document.getElementById("result");
  resultDiv.innerHTML = "";
};
